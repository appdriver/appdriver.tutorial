# 1, 2, fertig #
## Ajax-Frontend, REST-API, Java-Backend – vollständige Web-Anwendung in 3 Minuten #
## Tutorium mit Stefan Isele auf dem Herbstcampus 2014 in Nürnberg ##

Damit Sie aktiv im Tutorium mitarbeiten können brauchen Sie den entprechenden Quellcode und Java-Bibliotheken.
Wenn alle Teilnehmer am Montag morgen versuchen, diese über unser Netzwerk zu installieren, könnte es zu Engpässen kommen.
Daher möchten wir Sie bitten, dies bereits vorher zu tun.
Wahrscheinlich haben Sie Java, Maven, IDE und Browser bereits installiert, dann sollte dies nicht mehr als 5-10 Minuten in Anspruch nehmen.

* Rechner mit WLAN
* Java, ab 1.6
* Maven, ab Version 3
* Eine IDE, bevorzugt Eclipse 4 (Juno)  mit m2 Plugin für die Maven Integration
  Eclipse 3er Versionen oder andere IDEs sollten auch genügen, wenn sie über eine Maven Integration
  verfügen, dass wurde aber nicht getestet.
  Einen aktuellen Browser, für Javascript Debugging : Google Chrome, für SeleniumTests : Firefox
  Andere Browser z.B. Safari sollten auch genügen, allerdings können Sie dann evt. nur eingeschränkt 
  debuggen.
      Die Beispielprojekte laden Sie bitte vorab per Git von https://bitbucket.org/appdriver/appdriver.tutorial.git
      dann bauen Sie das Projekt einmal mit Maven, dadurch werden alle benötigten jars in das Maven Repository installiert :
      dazu wechseln Sie in das Verzeichnis appdriver.tutorial und geben ein : mvn package
      abschließend sollten Sie bitte die komplette WebAnwendung installieren, die wir im Tutorium verwenden.
      Eine Anleitung dafür finden Sie unter [http://appdriver.bitbucket.org/ll_CC/#!pages/tutorials/appdriver-create-web-application.md](http://appdriver.bitbucket.org/ll_CC/#!pages/tutorials/appdriver-create-web-application.md)
      das dauert nur ca. 3 Minuten. Wichtig ist nur, dass Sie diese Anwendung, wie dort beschrieben per Maven installieren und einmal mit mvn package bauen.


Sollten Sie bei den obigen Schritten auf Probleme stoßen, können Sie die gerne unter
https://bitbucket.org/appdriver/appdriver.tutorial/issues
melden, dann können wir diese ggf. noch rechtzeitig beheben.  

Vielen Dank für Ihre Mitarbeit, bis Montag !

Stefan Isele

[prefabware.de](http://www.prefabware.de)