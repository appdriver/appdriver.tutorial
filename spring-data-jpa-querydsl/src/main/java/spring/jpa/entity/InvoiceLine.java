package spring.jpa.entity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity
@Access(AccessType.FIELD)
public class InvoiceLine{
	@Id
	@GeneratedValue
	public Integer id;
	@Version
	public Integer version;
	public Integer number;
	
	@ManyToOne(fetch = FetchType.EAGER)
	public Product product;
	public Integer quantity;
	public InvoiceLine() {
		super();
	}

}