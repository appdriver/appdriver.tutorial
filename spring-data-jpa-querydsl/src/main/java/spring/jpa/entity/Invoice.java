package spring.jpa.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity
@Access(AccessType.FIELD)
public class Invoice  {
	@Id
	@GeneratedValue
	public Integer id;
	@Version
	public Integer version;
	public String code;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public List<InvoiceLine> lines = new ArrayList<InvoiceLine>();

	public boolean add(InvoiceLine e) {
		return lines.add(e);
	}

	
}
