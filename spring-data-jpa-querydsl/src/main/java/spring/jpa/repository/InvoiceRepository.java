package spring.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import spring.jpa.entity.Invoice;

public interface InvoiceRepository extends JpaRepository<Invoice,Integer>, QueryDslPredicateExecutor<Invoice> {
	
}
