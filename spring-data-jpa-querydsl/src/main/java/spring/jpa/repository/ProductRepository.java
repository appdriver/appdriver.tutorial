package spring.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import spring.jpa.entity.Product;

public interface ProductRepository extends JpaRepository<Product,Integer>, QueryDslPredicateExecutor<Product> {
	
}
