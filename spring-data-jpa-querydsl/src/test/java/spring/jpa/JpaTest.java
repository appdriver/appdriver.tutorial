package spring.jpa;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import spring.data.jpa.Config;
import spring.jpa.entity.Invoice;
import spring.jpa.entity.InvoiceLine;
import spring.jpa.entity.Product;
import spring.jpa.entity.QInvoice;
import spring.jpa.entity.QInvoiceLine;
import spring.jpa.entity.QProduct;
import spring.jpa.repository.InvoiceRepository;
import spring.jpa.repository.ProductRepository;

import com.mysema.query.types.expr.BooleanExpression;
/**
 * to test the jpa configuration
 * @author Stefan Isele
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {Config.class})
public class JpaTest {
	@Autowired
	private ProductRepository repository;
	@Autowired
	private InvoiceRepository invoiceRepository;
	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void testFindInvocie() throws Exception {
		Product p=createAndSave("product1", "4711");
		Product p2=createAndSave("product2", "4712");
		InvoiceLine l1 = createInvoiceLine(p);
		InvoiceLine l2 = createInvoiceLine(p2);
				
		Invoice i1=new Invoice();
		i1.add(l1);
		invoiceRepository.save(i1);
		
		QInvoice query= QInvoice.invoice;
		BooleanExpression contains = query.lines.any().product.name.contains("product1");
		Iterable<Invoice> result = invoiceRepository.findAll(contains);
		Iterator<Invoice> iterator = result.iterator();
		assertTrue(iterator.hasNext());
		Invoice invoice = iterator.next();
		assertFalse(iterator.hasNext());
		
	}

	InvoiceLine createInvoiceLine(Product p) {
		InvoiceLine l1=new InvoiceLine();
		l1.number=1;
		l1.product=p;
		l1.quantity=1;
		return l1;
	}
		@Test
		public void testFindOne() throws Exception {
		Product p=createAndSave("shoe42", "4711");
		assertTrue(p.id>0);
		QProduct query= QProduct.product;
		BooleanExpression nameContains = query.name.contains("oe");
		BooleanExpression numberEndsWith = query.number.endsWith("1");
		Iterable<Product> result = repository.findAll(nameContains.and(numberEndsWith));
		
		for (Product product : result) {
			assertTrue(product.name.contains("oe"));
			assertTrue(product.number.endsWith("1"));
		}
	}

	Product createAndSave( String name, String number) {
		Product p = new Product();
		p.name=name;
		p.number=number;
		repository.save(p);
		return p;
	}
	
	
	@After
	public void tearDown() {
	}

}
