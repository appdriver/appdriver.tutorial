package spring.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import spring.rest.store.entity.Product;

import com.prefabware.commons.CollectionUtil;
/**
 * run this test, copy the output from the console and 
 * paste it into
 * src\main\webapp\js\prefabware\app\crm\startupTypes.json
 * @author Stefan Isele
 *
 */
public class ModelCacheIntegrationTest {

	private FirefoxDriver driver;
	private Class<?>[] classes;

	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		classes=new Class[]{
				
			
				Product.class,
				
		};
	}
	
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
	String toJson(Class<?> entityClazz) {
		driver.navigate().to("http://localhost:8080/plugins-store/api/modelService/?typeName="+entityClazz.getName());
		List<WebElement> elements = driver.findElements(By.cssSelector("pre"));
		assertNotNull(elements);
		assertEquals(1,elements.size());
		WebElement element = elements.get(0);		
		return element.getText();
	}
	
	@Test
	public void test() {
		List<String>list=new ArrayList<String>();
		for (Class<?> entityClazz : classes) {
			list.add(toJson(entityClazz));
		}
		StringBuffer buf=new StringBuffer();
		buf.append("[");
		buf.append(CollectionUtil.seperatedBy(list, ","));
		buf.append("]");
		System.out.println(buf.toString());
	}
	

}
