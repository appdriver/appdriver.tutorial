package spring.rest.store;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import spring.rest.store.repository.ProductRepository;

@Configuration()
@Import({H2DataSourceConfig.class,HibernateEntityManagerConfig.class})
@EnableJpaRepositories(basePackageClasses = {
		 ProductRepository.class		
})
@EnableTransactionManagement
//@EnableAutoConfiguration
public class AppConfig {
	
 }