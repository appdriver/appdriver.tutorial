package spring.rest.store;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class WebConfig extends RepositoryRestMvcConfiguration {
	@Override
	protected void configureRepositoryRestConfiguration(
			RepositoryRestConfiguration config) {
		//if a request without mediatype arrives, use this one
		config.setDefaultMediaType(MediaType.APPLICATION_JSON);
		//its not enough to NOT annotate this class with @EnableHypermediaSupport
		//it would still use HAL
		//so we have to disable it here 
		config.useHalAsDefaultJsonMediaType(false);
	}
}