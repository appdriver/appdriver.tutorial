define('app/person/Person', [ 
       'dojo','dojo/date'], function(dojo,date) {
	dojo.declare("app.person.Person", null, {
		id:null,
		name:null,
		dateOfBirth:null,
		ageInYears : function(){
			return date.difference(this.dateOfBirth,new Date(),"year");
		}
	});
});
