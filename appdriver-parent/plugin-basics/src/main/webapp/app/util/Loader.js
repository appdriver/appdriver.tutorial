//to load a resource async
define('app/util/Loader', [ 
       'dojo','dojo/Deferred'], function(dojo) {
	dojo.declare("app.util.Loader", null, {
		load : function(fileName) {
			//async
			var deferred = new dojo.Deferred();
			var id="dojo/text!"+fileName;
			var that=this;
			require([id], function(file){
				that.content=file;
				if (file!=null) {
					deferred.resolve(file);
				}else{
					deferred.reject('could not find file '+id);
				};
				});
			return deferred.promise;
		},
	});
});
