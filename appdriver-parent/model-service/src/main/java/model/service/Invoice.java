package model.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import com.prefabware.meta.domain.annotation.Attribute;
import com.prefabware.meta.domain.annotation.EntityType;

@EntityType
public class Invoice {
	
	public Long id;
	public String code;
	public BigDecimal price;
	
	public Invoice() {
		super();
	}
	
	@Attribute(order = 10 , contained=true)
	@OneToMany(cascade = CascadeType.ALL)	
	@OrderBy("number ASC")
	public List<InvoiceLine> lines = new ArrayList<InvoiceLine>();
}
