package model.service;

import java.math.BigDecimal;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.prefabware.meta.domain.annotation.EntityType;

@EntityType
public class InvoiceLine {

	public Long id;
	
	public Integer number;

	@NotNull
	public BigDecimal quantity;

	public InvoiceLine() {
		super();
	}

	@ManyToOne(fetch = FetchType.EAGER)
	public Product product;

}
