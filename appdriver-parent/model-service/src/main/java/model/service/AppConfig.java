package model.service;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.prefabware.model.rest.server.ModelServerConfiguration;
import com.prefabware.rest.server.spring.RestServerConfiguration;

@Configuration()
@Import({ModelServerConfiguration.class,RestServerConfiguration.class})
public class AppConfig {
	
}