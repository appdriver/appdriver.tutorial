package model.service;

import java.math.BigDecimal;
import com.prefabware.meta.domain.annotation.EntityType;
import javax.validation.constraints.NotNull;

@EntityType
public class Product {
	
	public Long id;
	@NotNull
	public String name;
	@NotNull
	public BigDecimal price;
	public Product() {
		super();
	}
	
}
