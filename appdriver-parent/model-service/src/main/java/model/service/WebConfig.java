package model.service;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.prefabware.model.rest.server.web.ModelServerWebConfiguration;
import com.prefabware.rest.server.spring.RestWebMvcConfigureAdapter;

@Configuration
@Import({ModelServerWebConfiguration.class,RestWebMvcConfigureAdapter.class})
public class WebConfig extends WebMvcConfigurationSupport {
	
}