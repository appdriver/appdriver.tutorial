package model.service;

import static com.jayway.jsonassert.JsonAssert.with;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.prefabware.web.test.FirefoxTestSupport;
public class ModelIntegrationTest {

	private static FirefoxDriver driver;
	private static FirefoxTestSupport support;

	@BeforeClass
	public static void setUp() throws Exception {
		support = new FirefoxTestSupport().setUp();
		driver = support.driver;
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	}
	@AfterClass
	public static void tearDown() throws Exception {
		support.tearDown();
	}
	@Test
	public void testProduct() throws  Exception {
		String json = getJson("http://localhost:8080/api/modelService?typeName="+Product.class.getName());
		with(json)
		.assertThat("$.name", is(Product.class.getName()));
	}
	
	String getJson(String url) {   
		driver.navigate().to(url);
		List<WebElement> elements = driver.findElements(By.cssSelector("pre"));
		assertNotNull(elements);
		assertEquals(1,elements.size());
		WebElement element = elements.get(0);
		String json = element.getText();
		return json;
	}
}
