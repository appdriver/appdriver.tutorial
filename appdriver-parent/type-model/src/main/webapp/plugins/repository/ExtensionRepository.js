//to load resources like scripts and css 
define('plugins/repository/ExtensionRepository', [ 
       'dojo',
       "dojo/_base/lang",
       'prefabware/plugin/Extension',
       'dojo/promise/all',
       'dojo/_base/declare' ], function(
		dojo, lang,Extension,resolve) {
	dojo.declare("plugins.repository.ExtensionRepository", [prefabware.plugin.Extension], {
		loaderClass:null,//the classname of the loader
		entityClass:null,//the classname of the entity
		
		constructor : function(options) {
			this.loaderClass=options.json.loaderClass;
			this.entityClass=options.json.entityClass;
			this.resource   =options.json.resource;
		},	
		startup : function() {
			//async
			//create the instance of the resource loader
			var that=this;
			var p=this.loader.createInstance(this.loaderClass)
				.then(function(instance){
					that.resourceLoader=instance;
				});
			return p;
		},
		load : function(path) {
			var that=this;
			var both={};
			both.loader=this.loader.createInstance(this.loaderClass);	
			both.entity=this.loader.createInstance(this.entityClass);
			return resolve(both).then(function(resolved){
				return resolved.loader.load(that.resource).then(function(json){
					var obj = dojo.fromJson(json);
					lang.mixin(resolved.entity, obj);
					return resolved.entity;
				});
			});
		}
	});
});
