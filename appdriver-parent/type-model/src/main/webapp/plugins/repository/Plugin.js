//a view that can be shown in the workspace
define('plugins/repository/Plugin', [ 
       'dojo',
       'prefabware/plugin/Plugin',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("plugins.repository.Plugin", [prefabware.plugin.Plugin], {
		constructor : function() {
		},		
		findOne : function(entityClass) {
			//find the loader
			return this.fetchExtension("plugins.repository.repository").then(function(extension){
				return extension.load();
			});
		},
	});
});
