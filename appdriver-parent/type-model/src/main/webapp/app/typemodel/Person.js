define('app/typemodel/Person', [ 
       'dojo','dojo/date'], function(dojo,date) {
	dojo.declare("app.typemodel.Person", null, {
		id:null,
		name:null,
		dateOfBirth:null,
		ageInYears : function(){
			return date.difference(this.dateOfBirth,new Date(),"year");
		}
	});
});
