package spring.security.session;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> inMem = auth.inMemoryAuthentication();
	inMem.withUser("user").password("user").roles("USER");
	
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http 
    	.csrf().disable()//disable 
    	.authorizeRequests()
    	//the order of the rules is very important here !! first matching rule wins !!
        .antMatchers("/user-only.html").hasRole("USER")
        .antMatchers("/invalid-session.html").permitAll()    	 
        .antMatchers("/expired-session.html").permitAll()    	 
        .antMatchers("/login.html").permitAll()    	 
    	.antMatchers("/").permitAll()    	 
    	.antMatchers("/index.html").permitAll()    	 
    	.antMatchers("/index.css").permitAll()  
        .anyRequest().authenticated()
        .and()
        .formLogin().loginPage("/login.html").permitAll()
        .and()
        .httpBasic()
        .and()
        .sessionManagement()
        .invalidSessionUrl("/invalid-session.html")        
        .maximumSessions(1).expiredUrl("/expired-session.html");
        ;
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
       return super.authenticationManagerBean();
  }
}
