package spring.rest.service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
@Component
@SuppressWarnings("rawtypes")
 
public class ProductResourceAssembler extends ResourceAssemblerSupport<Product, Resource> {
    public ProductResourceAssembler() {
        super(ProductController.class, Resource.class);
    }
 
	@Override
    public List<Resource> toResources(Iterable<? extends Product> products) {
        List<Resource> resources = new ArrayList<Resource>();
        for(Product widget : products) {
            resources.add(new Resource<Product>(widget, linkTo(ProductController.class).slash(widget.id).withSelfRel()));
        }
        return resources;
    }
 
    public Resource toResource(Product product) {
        return new Resource<Product>(product, linkTo(ProductController.class).slash(product.id).withSelfRel());
    }
}