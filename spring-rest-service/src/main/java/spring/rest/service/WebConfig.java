package spring.rest.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebConfig extends WebMvcConfigurationSupport {
	@Bean
	public ProductResourceAssembler productResourceAssembler() {
		return new ProductResourceAssembler();
	}

	@Bean
	public ProductController productController() {
		return new ProductController();
	}

	@Bean()
	@Scope(value = org.springframework.web.context.WebApplicationContext.SCOPE_SESSION,
			proxyMode = ScopedProxyMode.INTERFACES)
	public ShoppingCart shoppingCart() {
		return new ShoppingCartImpl();
	};

	@Bean()
	public ShoppingCartController ShoppingCartController() {
		return new ShoppingCartController();
	}
}