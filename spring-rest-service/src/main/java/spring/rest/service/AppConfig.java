package spring.rest.service;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;

@Configuration()
@Import({})
public class AppConfig {
	@Bean
	ProductRepository products() {
		return new ProductRepository();
	}
	@Bean()
	@Scope(value =ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public ShoppingCartData shoppingCartData() {
		return new ShoppingCartData();
	};
}