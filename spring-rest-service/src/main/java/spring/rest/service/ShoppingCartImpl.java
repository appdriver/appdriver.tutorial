package spring.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

public class ShoppingCartImpl implements ShoppingCart {
	@Autowired
	ShoppingCartData data;

	public void add(Product product) {
		data.add(product);
	}

	public List<Product> getProducts() {
		return data.getProducts();
	}

	public ShoppingCartData getData() {
		return data;
	}
	
	
}
