package spring.rest.service;

import java.util.List;

public interface ShoppingCart {

	public abstract void add(Product product);

	public abstract List<Product> getProducts();

	public abstract ShoppingCartData getData();

}