package spring.rest.service;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(path="products",rel="products")
public class Product {
	
	public Long id;
	@NotNull
	public String name;
	@NotNull
	public BigDecimal price;
	public Product() {
		super();
	}
	
}
