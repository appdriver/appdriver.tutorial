package spring.rest.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 * to give the client access to the domain model
 * 
 * @author Stefan Isele
 * 
 */
@Controller
@SessionAttributes("shoppingCart")
@RequestMapping(value = "/shoppingCart", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
public class ShoppingCartController {


	public ShoppingCartController() {
		super();
	}

	/**
	 * we cannot use RequestBody parameter with get so we have to use post
	 * http://localhost:8080/api/products/map
	 * data = {"key1":"value1","key2":"value2"}
	 * 
	 * @param name
	 * @return the unchnged parameter e.g. {"key1":"value1","key2":"value2"}
	 */
	@RequestMapping(value = "map", method = RequestMethod.POST)
	public @ResponseBody
	Map<String, String> map(@RequestBody Map<String,String> values) {
		return values;
	}
	public static class Keys{
		//the fields must be public or wont get recognized as properties by Jackson
		public String[] texts;
		public String[] images;
	}
	public static class Results{
		public Map<String,String>texts=new HashMap<String, String>();
		public Map<String,String>images=new HashMap<String, String>();
	}
	
	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ResponseBody
	public String handleResourceNotFoundException(ResourceNotFoundException ex)
	{
		return ex.getMessage();
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String processValidationError(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
 
        return fieldErrors.toString();
    }
	@RequestMapping(value="addToCart",method = RequestMethod.PUT)
	public ResponseEntity<ShoppingCartData> addToCart(@RequestBody @Valid Product product) {
		shoppingCart.add(product);
		//we cannot return the ShoppingCart here, its a  proxy and has a lot of technical properties that
		//should not be included in the reponse
		//so we returen the data only
		return new ResponseEntity<ShoppingCartData>(shoppingCart.getData(), HttpStatus.OK);		
	}
	@Autowired
	ShoppingCart shoppingCart;
}
