package spring.rest.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * to give the client access to the domain model
 * 
 * @author Stefan Isele
 * 
 */
@Controller
@RequestMapping(value = "/products", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

	@Autowired
	ProductResourceAssembler resourceAssembler;

	@Autowired
	ProductRepository productRepository;

	public ProductController() {
		super();
	}

	/**
	 * uses a ResourceAssembler to map the return value
	 * contains a self link pointing to the roduct
	 * 
	 * @param name
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(params = "name", method = RequestMethod.GET)
	public @ResponseBody
	ResponseEntity<Resource<Product>> get(@RequestParam("name") String name) {
		Product product = find(name);
		return new ResponseEntity<Resource<Product>>(
				resourceAssembler.toResource(product), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody
	ResponseEntity<Boolean> delete(@PathVariable("id") long id) {
		productRepository.delete(id);
		// allways returns ok
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Resource<Product>> create(@RequestBody @Valid Product product) {
		return new ResponseEntity<Resource<Product>>(
				resourceAssembler.toResource(this.productRepository.save(product)), HttpStatus.OK);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Resource<Product>> update(@RequestBody @Valid Product product) {
		return new ResponseEntity<Resource<Product>>(
				resourceAssembler.toResource(this.productRepository.save(product)), HttpStatus.OK);
	}

	/**
	 * uses a ResourceAssembler to map the return value
	 * contains a self link pointing to the product
	 * 
	 * @param name
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	List<Resource> get() {
		return resourceAssembler.toResources(productRepository.findAll());

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody
	ResponseEntity<Resource<Product>> get(@PathVariable("id") long id) {
		Product product = find(id);
		return new ResponseEntity<Resource<Product>>(
				resourceAssembler.toResource(product), HttpStatus.OK);
	}

	/**
	 * uses a Resource, will not contain a self link
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "/resource", method = RequestMethod.GET)
	public @ResponseBody
	ResponseEntity<Resource<Product>> resource(@RequestParam("name") String name) {
		Product product = find(name);
		Resource<Product> resource = new Resource<Product>(product);
		return new ResponseEntity<Resource<Product>>(resource, HttpStatus.OK);
	}

	/**
	 * we cannot use RequestBody parameter with get so we have to use post
	 * http://localhost:8080/api/products/map
	 * data = {"key1":"value1","key2":"value2"}
	 * 
	 * @param name
	 * @return the unchnged parameter e.g. {"key1":"value1","key2":"value2"}
	 */
	@RequestMapping(value = "map", method = RequestMethod.POST)
	public @ResponseBody
	Map<String, String> map(@RequestBody Map<String,String> values) {
		return values;
	}
	public static class Keys{
		//the fields must be public or wont get recognized as properties by Jackson
		public String[] texts;
		public String[] images;
	}
	public static class Results{
		public Map<String,String>texts=new HashMap<String, String>();
		public Map<String,String>images=new HashMap<String, String>();
	}
	
	/**
	 * the values returned are the keys prefixed with "value "
	 * just to return something...
	 * @param keys
	 * {"texts":["text1","text2"],"images":["image1","images2"]}
	 * @return
	 * {"texts":{"text1":"valuetext1","text2":"valuetext2"},"images":{"image1":"valueimage1","images2":"valueimages2"}}
	 */
	@RequestMapping(value = "results", method = RequestMethod.POST)
	public @ResponseBody
	Results results(@RequestBody Keys keys) {
		
		Results results=new Results();
		for (String key : keys.texts) {
			results.texts.put(key, "value "+key);
		}
		for (String key : keys.images) {
			results.images.put(key, "value "+key);
		}
		return results;
	}
	/**
	 * uses an entity directly as return value
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "/pojo", method = RequestMethod.GET)
	public @ResponseBody
	Product pojo(@RequestParam("name") String name) {
		Product product = find(name);
		return product;
	}

	Product find(String name) {
		Product product = productRepository.findByName(name);
		if (product == null) { throw new ResourceNotFoundException("product with name=" + name + " not found."); }
		return product;
	}

	Product find(Long id) {
		Product product = productRepository.findById(id);
		if (product == null) { throw new ResourceNotFoundException("product with id=" + id + " not found."); }
		return product;
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ResponseBody
	public String handleResourceNotFoundException(ResourceNotFoundException ex)
	{
		return ex.getMessage();
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String processValidationError(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
 
        return fieldErrors.toString();
    }
	
}
