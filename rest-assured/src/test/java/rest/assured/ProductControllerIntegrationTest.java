package rest.assured;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * testing the REST api
 * with rest-assured
 * from https://code.google.com/p/rest-assured/
 * see https://code.google.com/p/rest-assured/w/list
 * for detailed documentation
 * 
 * @author Stefan Isele
 * 
 */
public class ProductControllerIntegrationTest {
	public static class JsonLink {
		public String rel;
		public String href;
	}

	public static class JsonProduct {
		public Long id;
		public String name;
		public BigDecimal price;
		public JsonLink[] links;
	}

	@BeforeClass
	public static void setUp() throws Exception {}

	@AfterClass
	public static void tearDown() throws Exception {}

	@Test
	public void testGetAll() {
		JsonProduct[] products = get("/api/products/").
				as(JsonProduct[].class);
		assertTrue(products.length > 0);
		JsonProduct p1 = products[0];
		assertTrue(p1.id > 0);
		assertFalse(p1.name.isEmpty());
		assertNotNull(p1.price);
		assertNotNull(p1.links);
		assertEquals(1, p1.links.length);
	}

	@Test
	public void testGetByName() {
		given().
				param("name", ProductRepository.HUT_NAME).
				get("/api/products").
				then().
				statusCode(200).
				body(
						"name", equalTo(ProductRepository.HUT_NAME)
				);
	}

}
