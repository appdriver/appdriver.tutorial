package rest.assured;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration()
public class AppConfig {
	
	@Bean
	ProductRepository products() {
		return new ProductRepository();
	}
}