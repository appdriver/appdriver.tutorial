package spring.jpa.entity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import org.springframework.data.rest.core.annotation.RestResource;

@Entity
@Access(AccessType.FIELD)
@RestResource(path="product",rel="product")
public class Product {
	/**
	 * must be Long not Integer, thats what Spring Rest expects 
	 */
	@Id
	@GeneratedValue
	public Long id;
	@Version
	public Integer version;
	public String number;
	public String name;
	public String code;
}
