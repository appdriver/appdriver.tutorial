package spring.data.jpa;

import org.springframework.beans.factory.annotation.Autowired;

import spring.jpa.entity.Product;
import spring.jpa.repository.ProductRepository;
import javax.annotation.PostConstruct;
public class TestData {

	@Autowired
ProductRepository productRepository;
	public TestData() {
		super();
	}
	@PostConstruct
void init(){
	Product p=new Product();
	p.name="nn";
	productRepository.save(p);
}
}
