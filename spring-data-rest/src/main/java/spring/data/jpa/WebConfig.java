package spring.data.jpa;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

@Configuration
public class WebConfig extends RepositoryRestMvcConfiguration {
}