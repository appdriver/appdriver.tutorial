package spring.jpa.service;

import spring.jpa.entity.Product;

public interface ProductService {
public Product save(Product p);
}
