package spring.jpa.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import spring.jpa.entity.Product;

public class ProductServiceImpl implements ProductService {
	@PersistenceContext
	private EntityManager entityManager;
	@Transactional
	public Product save(Product p) {
		entityManager.persist(p);
		return p;
	}

}
