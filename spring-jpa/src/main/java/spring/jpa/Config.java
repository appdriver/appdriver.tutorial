package spring.jpa;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import spring.jpa.service.ProductService;
import spring.jpa.service.ProductServiceImpl;


@Configuration()
@EnableTransactionManagement

@Import({H2DataSourceConfig.class,HibernateEntityManagerConfig.class})
public class Config {
@Bean
public ProductService productService(){return new ProductServiceImpl();}
 }