package spring.jpa;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import spring.jpa.entity.Product;
import spring.jpa.entity.QuantityUnit;
import spring.jpa.service.ProductService;
/**
 * to test the jpa configuration
 * @author Stefan Isele
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {Config.class})
public class JpaTest {
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	ProductService service;
	
	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	@Transactional
	public void testProduct() throws Exception {
		Product p = new Product();
		
		QuantityUnit qu=new QuantityUnit();
		qu.name="m";
		
		p.qu=qu;
		
		assertTrue(p.id==null);
		entityManager.persist(p);
		assertTrue(p.id>0);
	}
	
	@Test
	public void testService() throws Exception {
		Product p = new Product();
		assertTrue(p.id==null);
		service.save(p);
		assertTrue(p.id>0);
	}
	
	@After
	public void tearDown() {
	}

}
