define('app/PersonLoader', [ 
       'dojo',
       'app/util/Loader',
       'app/Person'
       ], function(dojo) {
	dojo.declare("app.PersonLoader", null, {
		loader:new app.util.Loader(),
		constructor : function(){
		},
		load : function(file){
			return this.loader.load(file).then(function(json){
				var obj = dojo.fromJson(json);
				var p=new app.Person();
				p.id=obj.id;
				p.name=obj.name;
				p.dateOfBirth=obj.dateOfBirth;
				return p;
			});
			
		}
	});
});
