package rest.assured;

import static com.jayway.jsonassert.JsonAssert.with;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * testing the REST api
 * with rest-assured
 * from https://code.google.com/p/rest-assured/
 * see https://code.google.com/p/rest-assured/w/list
 * for detailed documentation
 * 
 * @author Stefan Isele
 * 
 */
public class ProductControllerIntegrationTest {
	public static class JsonLink {
		public String rel;
		public String href;
	}

	public static class JsonProduct {
		public Long id;
		public String name;
		public BigDecimal price;
		public JsonLink[] links;
	}

	static FirefoxDriver driver;

	@BeforeClass
	public static void setUp() throws Exception {
		driver = new FirefoxDriver();
	}

	
	public static void tearDown() throws Exception {
		driver.close();
	}
	
	@Test
	public void testBrowserTest() {
		driver.navigate().to("http://localhost:8080");	
		WebElement h2 = driver.findElementByTagName("h2");
		assertNotNull(h2.getText());
		WebElement get = driver.findElementById("getButton");
		get.click();
		assertEquals("http://localhost:8080/",driver.getCurrentUrl());
		WebElement statusButton = driver.findElementById("statusButton");
		assertFalse(statusButton.isEnabled());

		WebElement statusText = driver.findElementById("statusText");
		assertEquals("200",statusText.getAttribute("value"));
		
	}

}
