package rest.assured;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductRepository {
	public static final String HUT_NAME = "Hut";
	public static final String JACKE_NAME = "Jacke";
	public static final String TASCHE_NAME = "Tasche";
	
	
	
	
	Map<String, Product> byName = new HashMap<String, Product>();
	Map<Long, Product> byId = new HashMap<Long, Product>();

	public ProductRepository() {
		super();
		addProduct(TASCHE_NAME, new BigDecimal("12.50"));
		addProduct(JACKE_NAME, new BigDecimal("57.30"));
		addProduct(HUT_NAME, new BigDecimal("19.90"));
	}

	public Product findByName(String name) {
		return byName.get(name);
	};
	public Product findById(Long id) {
		return byId.get(id);
	};
	public List<Product> findAll() {
		return new ArrayList<Product>(byName.values());
	};

	Product addProduct(String name, BigDecimal price) {
		Product p = new Product();
		p.name = name;
		p.price = price;
		return addProduct(p);
	}

	Product addProduct(Product p) {
		p.id = new Long(byName.size() + 1);
		byName.put(p.name, p);
		byId.put(p.id, p);
		return p;
	}

	public boolean delete(long id) {
		Product p =byId.remove(id);
		if (p==null) {
			return false;
		}else{
			byName.remove(p.name);
			return true;
		}
}

	public Product save(Product product) {
		addProduct(product);
		return product;
	}
	}

