package rest.assured;

import java.math.BigDecimal;

public class Product {
	
	public Long id;
	public String name;
	public BigDecimal price;
	public Product() {
		super();
	}
	
}
