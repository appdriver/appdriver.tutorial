package rest.assured;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebConfig extends WebMvcConfigurationSupport {
	@Bean
	public ProductResourceAssembler productResourceAssembler() {
		return new ProductResourceAssembler();
	}
	
	@Bean
	public ProductController productController() {
		return new ProductController();
	}
}