var app = angular.module('i18n-app', ['pascalprecht.translate']);

app.config(function ($translateProvider) {
  $translateProvider.useUrlLoader('api/resources');
  $translateProvider.preferredLanguage('en');
});

app.controller('i18nCtrl', function ($scope, $translate) {
  $scope.changeLanguage = function (key) {
    $translate.use(key);
  };
});