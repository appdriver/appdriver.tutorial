package thymeleaf;

import java.text.MessageFormat;
import java.util.Locale;

import org.springframework.context.support.AbstractMessageSource;

public class PojoMessageResource extends AbstractMessageSource {

	@Override
	protected MessageFormat resolveCode(String code, Locale locale) {
		if ("message.earth".equals(code)&&Locale.ENGLISH.equals(locale)) {
			return new MessageFormat("Earth");
		}
		else if ("message.earth".equals(code)&&Locale.GERMAN.equals(locale)) {
			return new MessageFormat("Erde");
		}
		else return null;
	}

}
