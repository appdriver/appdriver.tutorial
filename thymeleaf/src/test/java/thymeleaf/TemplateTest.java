package thymeleaf;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring3.SpringTemplateEngine;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = { ThymeleafConfig.class })
public class TemplateTest {

	@Before
	public void setUp() throws Exception {
	}

	@Autowired
	SpringTemplateEngine templateEngine;

	@Test
	public void testTemplate() throws IOException {
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("key1", "value1");
		String templateName = "templates/main.html";

		Locale locale = Locale.GERMAN;
		final Context ctx = new Context(locale);
		ctx.setVariables(variables);
		String result = this.templateEngine.process(templateName, ctx);
		assertTrue(result.contains("value1"));
		File tempFile = File.createTempFile("thymeleaf", ".html"); 
		PrintWriter out = new PrintWriter(tempFile,"UTF-8");
		out.println(result);
		out.close();
		//Desktop.getDesktop().open(tempFile);
	}

}
