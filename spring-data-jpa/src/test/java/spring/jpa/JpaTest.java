package spring.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import spring.data.jpa.Config;
import spring.jpa.entity.Product;
import spring.jpa.repository.ProductRepository;
/**
 * to test the jpa configuration
 * @author Stefan Isele
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {Config.class})
public class JpaTest {
	@Autowired
	private ProductRepository repository;
	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void testFindOne() throws Exception {
		Product p=createAndSave("shoe42", "4711");
		assertTrue(p.id>0);
		Product product = repository.findByNumber(p.number);
		assertEquals(p.number,product.number);
	}

	Product createAndSave( String name, String number) {
		Product p = new Product();
		p.name=name;
		p.number=number;
		repository.save(p);
		return p;
	}
	@Test
	public void testFindList() throws Exception {
		Product p=createAndSave("testFindList", "0001");
		assertTrue(p.id>0);
		List<Product> page = repository.findByNameLikeIgnoreCase(p.name);
		assertEquals(p.name,page.get(0).name);
	}
	@Test
	public void testFindByQuery() throws Exception {
		Product p=createAndSave("testFindByQuery", "0002");
		assertTrue(p.id>0);
		List<Product> list = repository.findByQuery(p.name, p.number);
		assertEquals(p.name,list.get(0).name);
	}
	@Test
	public void testFindPage() throws Exception {
		Product p=createAndSave("testFindPage", "0003");
		assertTrue(p.id>0);
		Pageable pageable=new PageRequest(0,10);
		Page<Product> page = repository.findPageByNameLikeIgnoreCase(p.name, pageable);
		assertEquals(p.name,page.getContent().get(0).name);
	}

	
	@After
	public void tearDown() {
	}

}
