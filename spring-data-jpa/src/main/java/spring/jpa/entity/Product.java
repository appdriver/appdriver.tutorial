package spring.jpa.entity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity
@Access(AccessType.FIELD)
public class Product {
	@Id
	@GeneratedValue
	public Integer id;
	@Version
	public Integer version;
	public String number;
	public String name;
}
