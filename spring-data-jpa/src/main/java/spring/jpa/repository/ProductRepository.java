package spring.jpa.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import spring.jpa.entity.Product;

public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
	public Product findByNumber(@Param("number")String number);
	
	@Query("select p from Product p where p.name = ?1 and p.number = ?2")
	public List<Product> findByQuery(String name,String number);
	
	public List<Product> findByNameLikeIgnoreCase(@Param("name")String name);
	public Page<Product> findPageByNameLikeIgnoreCase(@Param("name")String name,Pageable pageable);
}
