package spring.data.jpa;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import spring.jpa.repository.ProductRepository;


@Configuration()
@Import({H2DataSourceConfig.class,HibernateEntityManagerConfig.class})
@EnableJpaRepositories(basePackageClasses = {
		 ProductRepository.class		
})
@EnableTransactionManagement
public class Config {
	
 }