package spring.security.userdetails;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserService implements UserDetailsService {
	Map<String,MyUser>map=new HashMap<String, MyUser>();
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		MyUser user = map.get(username);
		boolean enabled=true;
		boolean accountNonExpired=true;
		boolean credentialsNonExpired=true;
		boolean accountNonLocked=true;
		return new org.springframework.security.core.userdetails.User(
				user.name, 
				user.encryptedPassword,
				enabled,
				accountNonExpired,
				credentialsNonExpired,
				accountNonLocked,
				getGrantedAuthorities());
	}
	Collection<? extends GrantedAuthority> getGrantedAuthorities() {
		//Caution role is here ROLE_USER not USER !
		return Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"));
	}
	public void add(MyUser u1) {
		map.put(u1.name, u1);
	}

}
