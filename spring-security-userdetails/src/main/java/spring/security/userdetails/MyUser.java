package spring.security.userdetails;

public class MyUser {
public MyUser(String name, String email, String encryptedPassword) {
		super();
		this.name = name;
		this.email = email;
		this.encryptedPassword = encryptedPassword;
	}
public String name;
public String email;
public String encryptedPassword;    
}
