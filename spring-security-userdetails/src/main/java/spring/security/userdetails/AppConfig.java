package spring.security.userdetails;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration()
@Import(SecurityConfig.class)
public class AppConfig {
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public UserService userService(PasswordEncoder passwordEncoder) {
		String pw1 = passwordEncoder.encode("user1");
		MyUser u1=new MyUser("user1","user@company.com",pw1); 
		UserService userService = new UserService();
		userService.add(u1); 
		return userService;
	}
 }