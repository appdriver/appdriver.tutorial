package spring;

import org.springframework.beans.factory.annotation.Autowired;


public class ServiceAImpl implements ServiceA {
	@Autowired
	Printer persistence;

	public void doSomething(String text) {
		persistence.print(text);
	}

}
