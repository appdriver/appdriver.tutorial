package spring.a02.resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import spring.Printer;
import spring.PrinterSystemErr;

@Configuration
public class ResourceConfig {
	@Bean
	public Printer printer() {
		return new PrinterSystemErr();
	}
	@Bean
	public UsingResource persistence(@Value("classpath:spring/a02/resource/resourceA.txt") Resource resource) {
		return new UsingResource(resource);
	}
    
 }