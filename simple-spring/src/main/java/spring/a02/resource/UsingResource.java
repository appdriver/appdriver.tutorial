package spring.a02.resource;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import spring.Printer;

public class UsingResource {
	@Autowired
	Printer printer;
	
	@Value("classpath:spring/a02/resource/resourceA.txt") 
	Resource resource;

	public UsingResource(Resource resource) {
		this.resource = resource;
	}

	public void print() {
		String string;
		try {
			string = new java.util.Scanner(new File(this.resource.getURI()), "UTF8").useDelimiter("\\Z").next();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		printer.print(string);

	}

}
