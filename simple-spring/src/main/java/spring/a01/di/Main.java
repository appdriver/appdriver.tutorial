package spring.a01.di;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import spring.ServiceA;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AnnotationConfigApplicationContext ctx = 
				   new AnnotationConfigApplicationContext(DiConfig.class);
		ServiceA serviceA = ctx.getBean(ServiceA.class);
		serviceA.doSomething("I am doing something !");
		ctx.close();
	}

}
