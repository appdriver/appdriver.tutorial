package spring.a01.di;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import spring.Printer;
import spring.PrinterSystemOut;
import spring.ServiceA;
import spring.ServiceAImpl;
@Configuration
public class DiConfig {

	@Bean
	public Printer printer() {
		return new PrinterSystemOut();
	}
	@Bean
	public ServiceA service() {
		return new ServiceAImpl();
	}
    
 }