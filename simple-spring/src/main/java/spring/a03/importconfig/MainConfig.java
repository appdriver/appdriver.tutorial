package spring.a03.importconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import spring.Printer;
import spring.PrinterSystemOut;
import spring.ServiceB;
import spring.ServiceBImpl;

@Configuration
@Import(ImportedConfig.class)
public class MainConfig {

	@Bean
	public Printer printer() {
		return new PrinterSystemOut();
	}
	@Bean
	public ServiceB serviceB() {
		return new ServiceBImpl();
	}
    
 }