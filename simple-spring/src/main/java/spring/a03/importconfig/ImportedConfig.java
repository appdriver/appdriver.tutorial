package spring.a03.importconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import spring.ServiceA;
import spring.ServiceAImpl;

@Configuration
public class ImportedConfig {
	
	@Bean
	public ServiceA serviceA() {
		return new ServiceAImpl();
	}
    
 }