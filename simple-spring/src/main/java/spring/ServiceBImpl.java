package spring;

import org.springframework.beans.factory.annotation.Autowired;


public class ServiceBImpl implements ServiceB {
	@Autowired
	Printer persistence;

	public void doSomething(String text) {
		persistence.print(text);
	}

}
