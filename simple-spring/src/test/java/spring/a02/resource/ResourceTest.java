package spring.a02.resource;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * to test the jpa configuration
 * 
 * @author Stefan Isele
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ResourceConfig.class })
public class ResourceTest {
	@Autowired
	UsingResource usingResource;

	@Before
	public void setUp() throws Exception {}

	@Test
	public void testPrint() throws Exception {
		assertNotNull(usingResource.resource);
		usingResource.print();
	}

	@After
	public void tearDown() {}

}
